﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tutorial2.model
{
    public class Book
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string CoverImage { get; set; }
    }
    public class BookManager
    {
        public static List<Book> GetBooks()
        {
            var books = new List<Book>();
            books.Add(new Book { BookId = 1, Title = "Tulpate", Author = "giang", CoverImage = "Assets/Books/1.png" });
            books.Add(new Book { BookId = 2, Title = "Mazin", Author = "kiên", CoverImage = "Assets/Books/2.png" });
            books.Add(new Book { BookId = 3, Title = "Elit", Author = "chiến", CoverImage = "Assets/Books/3.png" });
            books.Add(new Book { BookId = 4, Title = "Etiam", Author = "dũng", CoverImage = "Assets/Books/4.png" });
            books.Add(new Book { BookId = 5, Title = "Norummy", Author = "toàn", CoverImage = "Assets/Books/5.png" });
            books.Add(new Book { BookId = 6, Title = "Nostrud", Author = "phong", CoverImage = "Assets/Books/6.png" });
            books.Add(new Book { BookId = 7, Title = "Per Modo", Author = "trường", CoverImage = "Assets/Books/7.png" });
            books.Add(new Book { BookId = 8, Title = "Suscipit", Author = "tú", CoverImage = "Assets/Books/8.png" });
            books.Add(new Book { BookId = 9, Title = "Erat", Author = "ninh", CoverImage = "Assets/Books/9.png" });
            books.Add(new Book { BookId = 10, Title = "Consequat", Author = "đạt", CoverImage = "Assets/Books/10.png" });
            books.Add(new Book { BookId = 11, Title = "Aliquip", Author = "hằng", CoverImage = "Assets/Books/11.png" });
            books.Add(new Book { BookId = 12, Title = "Decima", Author = "tú", CoverImage = "Assets/Books/12.png" });
            books.Add(new Book { BookId = 13, Title = "Feugait", Author = "linh", CoverImage = "Assets/Books/13.png" });

            return books;
        }

    }
}
