﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using newapi.Models;
using newapi.Models;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Collections.ObjectModel;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace newapi
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        ObservableCollection<Article> Articles;
        public MainPage()
        {
            this.InitializeComponent();
            Articles = new ObservableCollection<Article>();
            InitJSON();
        }
        private async void InitJSON()
        {
            var url = "https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=52a380e5dfed4a1ca1761f007b2b9343";
            var list = await news.GetNews(url) as news;
            list.articles.ForEach(it =>
            {
                Articles.Add(it);
            });
        }
        private void view_detail(object sender, ItemClickEventArgs e)
        {
            Article ar = e.ClickedItem as Article;
            Frame.Navigate(typeof(detail),ar);
        }
    }
}
