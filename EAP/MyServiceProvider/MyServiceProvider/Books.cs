﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace MyServiceProvider
{
    public class Books
    {
        [DataMember]
        public int BookId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string ISBN { get; set; }
    }
    public interface iBookRepository
    {
        List<Book> GetBooks();
        Book AddNewBook(Book item);
    }
    public class BookRepository: iBookRepository
    {
        private List<Book> books = new List<Book>();
        public List<Book> GetAllBook()
        {
            return books;
        }
        public Book AddNewBook(Book item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Book fail");
            }
            item.BookId =  1;
            books.Add(item);

            return 
        }
    }
}