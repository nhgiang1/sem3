﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankService
{
    public class CreeateTransaction
    {
        public int partnerCode { get; set; }
        public int partnerPassword { get; set; }
        public int customerCode { get; set; }
        public int customerPin { get; set; }
        public string transactionName { get; set; }
        public decimal transactionAmount { get; set; }
        public int transactionCode { get; set; }
    }
    public class CustomerTransaction
    {
        public int customerCode { get; set; }
        public int customerPin { get; set; }
    }
    public class PartnerTransaction
    {
        public int partnerCode { get; set; }
        public int partnerPassword { get; set; }
    }
    public class ListTransOfCus
    {
        public int customerCode { get; set; }
        public string transactionName { get; set; }
        public decimal transactionAmount { get; set; }
        public int transactionCode { get; set; }
    }
    public class ListTransOfPart
    {
        public int partnerCode { get; set; }
        public string transactionName { get; set; }
        public decimal transactionAmount { get; set; }
        public int transactionCode { get; set; }
    }
    public class BankAction
    {
        BankDataContext db = new BankDataContext();
        public TransactionHistrory FormTransactionCus(
            int customerId,
            int transctionCode,
            decimal transctionAmount,
            string transctionName)
        {
            TransactionHistrory transaction = new TransactionHistrory();
            transaction.customerId = customerId;
            transaction.transactionCode = transctionCode;
            transaction.transactionAmount = (int)transctionAmount;
            transaction.transactionName = transctionName;
            return transaction;
        }
        public TransactionHistrory FormTransactionPart(
            int partnerId,
            int transctionCode,
            decimal transctionAmount,
            string transctionName)
        {
            TransactionHistrory transaction = new TransactionHistrory();
            transaction.partnerId = partnerId;
            transaction.transactionCode = transctionCode;
            transaction.transactionAmount = (int)transctionAmount;
            transaction.transactionName = transctionName;
            return transaction;
        }

        public bool CreateTrans(CreeateTransaction creeateTransaction)
        {
            try
            {
                Customer cusTrans =
                    (from cus in db.Customers where cus.customerCode == creeateTransaction.customerCode 
                     && cus.cusstomerPin == creeateTransaction.customerPin select cus).DefaultIfEmpty().Single();
                Partner partTrans =
                    (from part in db.Partners
                     where part.partnerCode == creeateTransaction.partnerCode
                    && part.partnerPassword == creeateTransaction.partnerPassword select part).DefaultIfEmpty().Single();

                if (cusTrans != null)
                {
                    if (creeateTransaction.transactionAmount >0)
                    {
                        cusTrans.customerBalance -= (int)creeateTransaction.transactionAmount;
                    }
                    else
                    {
                        cusTrans.customerBalance += (int)creeateTransaction.transactionAmount;
                    }
                    db.TransactionHistrories.InsertOnSubmit(
                        FormTransactionCus(
                            cusTrans.customerCode,
                            creeateTransaction.transactionCode,
                            creeateTransaction.transactionAmount,
                            creeateTransaction.transactionName)
                        );
                    db.SubmitChanges();
                }else if (partTrans != null)
                {
                    if (creeateTransaction.transactionAmount > 0)
                    {
                        partTrans.partnerBalance -= (int)creeateTransaction.transactionAmount;
                    }
                    else
                    {
                        partTrans.partnerBalance += (int)creeateTransaction.transactionAmount;
                    }
                    db.TransactionHistrories.InsertOnSubmit(
                        FormTransactionPart(
                            partTrans.partnerId,
                            creeateTransaction.transactionCode,
                            creeateTransaction.transactionAmount,
                            creeateTransaction.transactionName)
                        );
                    db.SubmitChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<ListTransOfCus> CustomerTransaction(CustomerTransaction data)
        {
            try
            {
                Customer cusTrans = (from cus in db.Customers where cus.cusstomerPin == data.customerPin && cus.customerCode == data.customerCode select cus).DefaultIfEmpty().Single();
                if (cusTrans != null)
                {
                    List<ListTransOfCus> lists = new List<ListTransOfCus>();
                    List<TransactionHistrory> listTrans =
                        (from translist in db.TransactionHistrories where translist.customerId == cusTrans.customerId select translist).ToList();
                    foreach (TransactionHistrory trans in listTrans)
                    {
                        ListTransOfCus list = new ListTransOfCus();
                        list.customerCode = (int)trans.customerId;
                        list.transactionCode = trans.transactionCode;
                        list.transactionName = trans.transactionName;
                        list.transactionAmount = trans.transactionAmount;
                        lists.Add(list);
                    }

                    return lists;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public List<ListTransOfPart> PartnerTransaction(PartnerTransaction data)
        {
            try
            {
                Partner parrtTrans = (from part in db.Partners where part.partnerPassword == data.partnerPassword && part.partnerCode == data.partnerCode select part).DefaultIfEmpty().Single();
                if (parrtTrans != null)
                {
                    List<ListTransOfPart> lists = new List<ListTransOfPart>();
                    List<TransactionHistrory> listTrans =
                        (from translist in db.TransactionHistrories where translist.partnerId == parrtTrans.partnerId select translist).ToList();
                    foreach (TransactionHistrory trans in listTrans)
                    {
                        ListTransOfPart list = new ListTransOfPart();
                        list.partnerCode = (int)trans.partnerId;
                        list.transactionCode = trans.transactionCode;
                        list.transactionName = trans.transactionName;
                        list.transactionAmount = trans.transactionAmount;
                        lists.Add(list);
                    }
                    return lists;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }
    }
}