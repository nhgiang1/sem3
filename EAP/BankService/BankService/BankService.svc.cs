﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BankService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BankService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select BankService.svc or BankService.svc.cs at the Solution Explorer and start debugging.
    public class BankService : IBankService
    {
        BankAction action = new BankAction();

        public bool CreeateTransaction(CreeateTransaction creeateTransaction)
        {
            return action.CreateTrans(creeateTransaction);
        }

        public List<ListTransOfCus> CustomerTransaction(CustomerTransaction customerTransaction)
        {
            return action.CustomerTransaction(customerTransaction);
        }

        public List<ListTransOfPart> PartnerTransaction(PartnerTransaction partnerTransaction)
        {
            return action.PartnerTransaction(partnerTransaction);
        }
    }
}
