﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Net;

namespace ConsumingRESTServiceCRUD_Client.Models
{
    public class BookServiceClient
    {
        String BASE_URL = "http://localhost:53705/BookServices.svc/";
        // I just call one method "GETALLBooks" only
        public List<Book> GetAllBook()
        {
            var syncClient = new WebClient();
            var content = syncClient.DownloadString(BASE_URL + "Books");
            var json_serializer = new JavaScriptSerializer();
            return json_serializer.Deserialize<List<Book>>(content);

        }

    }
}