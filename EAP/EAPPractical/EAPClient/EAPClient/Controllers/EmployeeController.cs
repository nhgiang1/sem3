﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EAPClient.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeService.EmployeeServiceClient empSer = new EmployeeService.EmployeeServiceClient(); 

        public ActionResult Search()
        {
            ViewBag.employee = null;
            return View();
        }

        [HttpPost]
        public ActionResult Search(Models.Search depart)
        {
            try
            {
                ViewBag.employee = empSer.SearchEmployeeByDepartment(depart.depart);
                return View();
            }
            catch
            {
                ViewBag.employee = null;
                return View();
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(EmployeeService.Employee eml)
        {
            try
            {
                var client = empSer.AddEmployee(eml);
                if (client)
                {
                    return RedirectToAction("Search");
                }
                else
                {
                    return View();
                }
            }
            catch
            {
                return View();
            }
        }
    }
}