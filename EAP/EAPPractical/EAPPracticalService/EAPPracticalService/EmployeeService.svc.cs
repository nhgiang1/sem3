﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.Linq.SqlClient;

namespace EAPPracticalService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeService.svc or EmployeeService.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeService : IEmployeeService
    {
        EmployeeDataContext db = new EmployeeDataContext();
        
        public bool AddEmployee(Employee emp)
        {
            try
            {
                db.Employees.InsertOnSubmit(emp);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public List<Employee> SearchEmployeeByDepartment(string depart)
        {
            try
            {
                List<Employee> empSearch =
                    (from listSearch in db.Employees where SqlMethods.Like(listSearch.department, "%" + depart + "%") select listSearch).ToList();
                return empSearch;
            }
            catch
            {
                return null;
            }

        }
    }
}
